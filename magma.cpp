// ConsoleApplication2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "stdio.h"
#include "stdint.h"
#include "memory.h"

typedef uint8_t substitution_t[128];

substitution_t pi = {
	0xc, 0x4, 0x6, 0x2, 0xa, 0x5, 0xb, 0x9, 0xe, 0x8, 0xd, 0x7, 0x0, 0x3, 0xf, 0x1,
	0x6, 0x8, 0x2, 0x3, 0x9, 0xa, 0x5, 0xc, 0x1, 0xe, 0x4, 0x7, 0xb, 0xd, 0x0, 0xf,
	0xb, 0x3, 0x5, 0x8, 0x2, 0xf, 0xa, 0xd, 0xe, 0x1, 0x7, 0x4, 0xc, 0x9, 0x6, 0x0,
	0xc, 0x8, 0x2, 0x1, 0xd, 0x4, 0xf, 0x6, 0x7, 0x0, 0xa, 0x5, 0x3, 0xe, 0x9, 0xb,
	0x7, 0xf, 0x5, 0xa, 0x8, 0x1, 0x6, 0xd, 0x0, 0x9, 0x3, 0xe, 0xb, 0x4, 0x2, 0xc,
	0x5, 0xd, 0xf, 0x6, 0x9, 0x2, 0xc, 0xa, 0xb, 0x7, 0x8, 0x1, 0x4, 0x3, 0xe, 0x0,
	0x8, 0xe, 0x2, 0x5, 0x6, 0x9, 0x1, 0xc, 0xf, 0x4, 0xb, 0x0, 0xd, 0xa, 0x3, 0x7,
	0x1, 0x7, 0xe, 0xd, 0x0, 0x5, 0x8, 0x3, 0x4, 0xf, 0xa, 0x6, 0x9, 0xc, 0xb, 0x2
};

void magma_round(uint32_t round_key, uint32_t* a1, uint32_t a0)
{
	uint32_t g = a0 + round_key;

	uint32_t t =
		  ((pi[0   + ((g & 0x0000000f) >> 0)]) << 0)
		| ((pi[16  + ((g & 0x000000f0) >> 4)]) << 4)
		| ((pi[32  + ((g & 0x00000f00) >> 8)]) << 8)
		| ((pi[48  + ((g & 0x0000f000) >> 12)]) << 12)
		| ((pi[64  + ((g & 0x000f0000) >> 16)]) << 16)
		| ((pi[80  + ((g & 0x00f00000) >> 20)]) << 20)
		| ((pi[96  + ((g & 0x0f000000) >> 24)]) << 24)
		| ((pi[112 + ((g & 0xf0000000) >> 28)]) << 28);

	*a1 ^= ((t << 11) | (t >> 21));
}

uint64_t magma_encrypt_block(uint32_t *key, uint64_t block)
{
	uint32_t inputa1 = block >> 32;
	uint32_t inputa0 = block;
	uint32_t tmp = 0;

	printf("ENCRYPT\n%2.x\n%2.x\n", inputa1, inputa0);

	for (int i = 0; i < 32; i++)
	{
		magma_round(key[i], &inputa1, inputa0);
		tmp = inputa1;
		inputa1 = inputa0;
		inputa0 = tmp;
		printf("ROUND%d\n%2.x\n%2.x\n", i, inputa1, inputa0);
	}

	uint64_t out = inputa0;
	out = out << 32;
	out += inputa1;

	inputa1 = out >> 32;
	inputa0 = out;

	printf("ENCRYPTEND\n%2.x\n%2.x\n\n", inputa1, inputa0);

	return out;
}

uint64_t magma_decrypt_block(uint32_t *key, uint64_t block)
{
	uint32_t inputa1 = block >> 32;
	uint32_t inputa0 = block;
	uint32_t tmp = 0;

	printf("DECRYPT\n%2.x\n%2.x\n", inputa1, inputa0);

	for (int i = 0; i < 32; i++)
	{
		magma_round(key[31-i], &inputa1, inputa0);
		tmp = inputa1;
		inputa1 = inputa0;
		inputa0 = tmp;
		printf("ROUND%d\n%2.x\n%2.x\n", i, inputa1, inputa0);
	}

	uint64_t out = inputa0;
	out = out << 32;
	out += inputa1;

	inputa1 = out >> 32;
	inputa0 = out;

	printf("DECRYPTEND\n%2.x\n%2.x\n", inputa1, inputa0);

	return out;
}

int test_magma_encrypt_decrypt_block()
{
	uint32_t K[32] =
	{
		0xffeeddcc, 0xbbaa9988, 0x77665544, 0x33221100,
		0xf0f1f2f3, 0xf4f5f6f7, 0xf8f9fafb, 0xfcfdfeff,

		0xffeeddcc, 0xbbaa9988, 0x77665544, 0x33221100,
		0xf0f1f2f3, 0xf4f5f6f7, 0xf8f9fafb, 0xfcfdfeff,

		0xffeeddcc, 0xbbaa9988, 0x77665544, 0x33221100,
		0xf0f1f2f3, 0xf4f5f6f7, 0xf8f9fafb, 0xfcfdfeff,

		0xfcfdfeff, 0xf8f9fafb, 0xf4f5f6f7, 0xf0f1f2f3,
		0x33221100, 0x77665544, 0xbbaa9988, 0xffeeddcc
	};

	uint64_t input = 0xfedcba9876543210;
	uint32_t inputa1 = input >> 32;
	uint32_t inputa0 = input;
	uint64_t enoutput = 0, deoutput = 0;

	printf("TEST\n%2.x\n%2.x\n\n", inputa1, inputa0);
	enoutput = magma_encrypt_block(K, input);
	deoutput = magma_decrypt_block(K, enoutput);
	
	//FINISH TEST OUTPUT

	inputa1 = enoutput >> 32;
	inputa0 = enoutput;
	printf("\nFINTESTENCRYPT\n%2.x\n%2.x\n\n", inputa1, inputa0);

	inputa1 = deoutput >> 32;
	inputa0 = deoutput;
	printf("FINTESTDECRYPT\n%2.x\n%2.x\n\n", inputa1, inputa0);

	return 0;
}

int main()
{
	printf("START\n\n");
	test_magma_encrypt_decrypt_block();
	printf("\nFINISH");
	getchar();

	return 0;
}